
import { combineReducers } from 'redux';

import drawer from './drawer';
import user from './user';
import list from './list';
import routes from './routes';

export default combineReducers({
  drawer,
  user,
  list,
  routes
});
