import type { Action } from '../actions/types';
import { LOGGED_IN,SKIP_INTRO,LOGGED_OUT,SKIP_LOGIN} from '../actions/user';
import api from '../byfus';

export type State = {
  hasLoggedIn: boolean,
  hasSkippedLogin: boolean,
  hasSkipedIntro:boolean,
}

const initialState = {
   isLoading:false,
   hasLoggedIn: false,
   hasSkippedLogin: false,
   hasSkipedIntro:false,
};

export default function (state:State = initialState, action:Action): State {
  if (action.type === LOGGED_IN) {
    return {
      ...state,
      hasLoggedIn:true,
      hasSkippedLogin:true,
    };
  }else if(action.type===SKIP_INTRO){
    return {
      ...state,
      hasSkipedIntro: action.payload,
    };
  }else if(action.type===SKIP_LOGIN){
    return {
      ...state,
      hasSkippedLogin: action.payload,
    };
  }else if (action.type === LOGGED_OUT) {
    return initialState;
  }
  return state;
}
