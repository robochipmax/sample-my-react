const React = require('react-native');

export default {
    id : 'scwwgocs8sw408sw8o4g00wk8wws0cccgoooo0s0',
    url : 'http://192.168.56.1/coreByfus',
    asset_upload:'http://192.168.56.1/coreByfus/assets/upload/',
    url_api :'http://192.168.56.1/coreByfus/api/',
    _get(path){
      var params = {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'X-CONNECTION-STAMP':this.id
        },
      };
        var fetching =   fetch(this.url_api+path,params).then((res)=>res.json()).catch((error)=>{
          console.error(error);
        });
      return fetching;
    },
    _post(path,content){
     var formData = new FormData();
     _.each(content, (p,i) => {
       formData.append(i,p);
     })
      var params = {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
          'X-CONNECTION-STAMP': this.id
        },
        body:formData
      };
      var fetching =   fetch(this.url_api+path,params).then((res)=>res.json()).catch((error)=>{
        console.error(error);
      });
    return fetching;
    },
}
