import React, { Component } from 'react';
import { BackAndroid,StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { Drawer } from 'native-base';
import { Router, Scene,Actions,ActionConst} from 'react-native-router-flux';
import { closeDrawer } from './actions/drawer';
import Intro from './components/intro/';
import SideBar from './components/sideBar';
import Main from './components/main/';
import Login from './components/login/';
import Register from './components/register/';
import Forget from './components/forget/';
import Explorer from './components/explorer/';

import Request from './components/request/';
import RequestDetail from './components/request/detail';
import Trip from './components/trip/';
import Recommendation from './components/recommendation/';
import Connection from './components/connection/';
import Inbox from './components/inbox/';
import Setting from './components/setting/';
import SplashPage from './components/splashscreen/';
import { statusBarColor } from './themes/base-theme';

const RouterWithRedux = connect()(Router);
const _renderScene = Actions.create(
   <Scene key='root' hideNavBar={true}>
       <Scene key='splash' component={SplashPage} initial={true}/>
       <Scene key='intro' component={Intro} />
       <Scene key='login' component={Login}/>
       <Scene key='main' component={Main} type= {ActionConst.REPLACE} />
       <Scene key='explorer' component={Explorer} type= {ActionConst.REPLACE}/>
       <Scene key='request' component={Request} type= {ActionConst.REPLACE}/>
       <Scene key='recommendation' component={Recommendation} type= {ActionConst.REPLACE}/>
       <Scene key='request_detail' component={RequestDetail} />
       <Scene key='trip' component={Request} type= {ActionConst.REPLACE}/>
       <Scene key='setting' component={Setting} type= {ActionConst.REPLACE}/>
       <Scene key='inbox' component={Inbox} type= {ActionConst.REPLACE}/>
       <Scene key='connection' component={Connection} type= {ActionConst.REPLACE}/>
       <Scene key='register' component={Register} />
       <Scene key='forget' component={Forget} />
   </Scene>);

class AppNavigator extends Component {

    static propTypes = {
      drawerState: React.PropTypes.string,
      user:React.PropTypes.object,
      closeDrawer: React.PropTypes.func,
      routes:React.PropTypes.object,
    }

  componentDidUpdate() {
    if (this.props.drawerState === 'opened') {
      this.openDrawer();
    }

    if (this.props.drawerState === 'closed') {
      this._drawer._root.close();
    }
  }

  componentDidMount() {
    BackAndroid.addEventListener('hardwareBackPress', () => {
      const scene = this.props.routes.scene;
      if (scene.sceneKey == 'main' || scene.sceneKey == 'login') {
        return () => false;
      }
      Actions.pop(scene.sceneKey);
      return () => true;
    });
  }

  openDrawer() {
    this._drawer._root.open();
  }

  closeDrawer() {
    if (this.props.drawerState === 'opened') {
      this.props.closeDrawer();
    }
  }


  render() {
    return (
      <Drawer
        ref={(ref) => { this._drawer = ref; }}
        type="overlay"
        tweenDuration={150}
        content={<SideBar navigator={this._navigator} />}
        tapToClose
        acceptPan={false}
        onClose={() => this.closeDrawer()}
        openDrawerOffset={0.2}
        panCloseMask={0.2}
        styles={{
          drawer: {
            shadowColor: '#000000',
            shadowOpacity: 0.8,
            shadowRadius: 3,
          },
        }}
        tweenHandler={(ratio) => {  //eslint-disable-line
          return {
            drawer: { shadowRadius: ratio < 0.2 ? ratio * 5 * 5 : 5 },
            main: {
              opacity: (2 - ratio) / 2,
            },
          };
        }}
        negotiatePan>
  
        <RouterWithRedux scenes={_renderScene}/>
      </Drawer>
    );
  }
}

function bindAction(dispatch) {
  return {
    closeDrawer: () => dispatch(closeDrawer()),
  };
}

const mapStateToProps = state => ({
  drawerState: state.drawer.drawerState,
  routes:state.routes
});

export default connect(mapStateToProps, bindAction)(AppNavigator);
