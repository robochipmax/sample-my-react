import React, { Component } from 'react';
import {Image,View} from 'react-native';
import { connect } from 'react-redux';
import { Content, Text, ListItem,H1,Left,Icon,Body,Thumbnail} from 'native-base';
import { Actions } from 'react-native-router-flux';
import StarRating from 'react-native-star-rating';
import { closeDrawer } from '../../actions/drawer';
import { setIndex } from '../../actions/list';
import styles from './style';

class SideBar extends Component {

  static propTypes = {
    closeDrawer: React.PropTypes.func,
  }

  constructor(props){
    super(props);
    console.log(props);
  }

  render() {
    return (
      <Content style={styles.sidebar} >
        <Image  style={{height:150,flex:1,justifyContent:'center',padding:10}} source={require('../../../images/side-bg.jpg')} stretch>
             <Thumbnail source={require('../../../images/header-logo.png')} />
             <Text style={{color:'#FFF'}}>FullName</Text>
             <View style={{width:100,marginTop:10}}>
             <StarRating
                disabled={true}
                starSize={20}
                starColor={'orange'}
                rating={5}
              />
              </View>
        </Image>
        <View style={{marginLeft:-10}}>
        <ListItem button icon onPress={() => { Actions.main();this.props.closeDrawer(); }} >
            <Left>
                <Icon name="ios-home-outline" />
            </Left>
            <Body>
              <Text>Home</Text>
            </Body>
        </ListItem>
        <ListItem button icon onPress={() => { Actions.explorer();this.props.closeDrawer(); }} >
            <Left>
                <Icon name="ios-folder-open-outline" />
            </Left>
            <Body>
              <Text>Explorer</Text>
            </Body>
        </ListItem>
        <ListItem itemDivider>
             <Text>Account</Text>
         </ListItem>
        <ListItem button icon onPress={() => { Actions.request();this.props.closeDrawer(); }} >
          <Left>
            <Icon name="ios-create-outline"/>
          </Left>
          <Body>
            <Text>Request</Text>
          </Body>
        </ListItem>
        <ListItem button icon onPress={() => { Actions.trip();this.props.closeDrawer(); }} >
          <Left>
            <Icon name="ios-jet-outline"/>
          </Left>
          <Body>
            <Text>Trip</Text>
          </Body>
        </ListItem>
        <ListItem button icon onPress={() => { Actions.recommendation();this.props.closeDrawer(); }} >
          <Left>
            <Icon name="ios-thumbs-up-outline"/>
          </Left>
          <Body>
            <Text>Recommendation</Text>
          </Body>
        </ListItem>
        <ListItem button icon onPress={() => { Actions.connection();this.props.closeDrawer(); }} >
          <Left>
            <Icon name="ios-contacts-outline"/>
          </Left>
          <Body>
            <Text>Connections</Text>
          </Body>
        </ListItem>
        <ListItem button icon onPress={() => {  Actions.inbox();this.props.closeDrawer(); }} >
          <Left>
            <Icon name="ios-mail-open-outline"/>
          </Left>
          <Body>
            <Text>Inbox</Text>
          </Body>
        </ListItem>
        <ListItem button icon onPress={() => { Actions.intro();this.props.closeDrawer(); }} >
          <Left>
            <Icon name="ios-settings-outline"/>
          </Left>
          <Body>
            <Text>Setting</Text>
            </Body>
        </ListItem>
        </View>
      </Content>
    );
  }
}

function bindAction(dispatch) {
  return {
    closeDrawer: () => dispatch(closeDrawer()),
    navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
  };
}

export default connect(null, bindAction)(SideBar);
