
const React = require('react-native');

const { StyleSheet } = React;

export default {
  sidebar: {
    flex: 1,
    backgroundColor: '#f8f8f8',
  },
};
