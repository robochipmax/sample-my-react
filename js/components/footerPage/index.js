
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Footer, FooterTab, Button, Icon, Badge, Text } from 'native-base';

import styles from './styles';

class FooterPage extends Component {

  static propTypes = {
  }
  constructor(props){
    super(props);
    this.state = {activeTabName:'home'};
  }
  tabAction(tab){
    this.setState({
      activeTabName:tab
    });
    if(tab  === 'home'){
      Actions.home();
    }else if(tab  === 'explorer'){
      Actions.explorer();
    }else if(tab  === 'setting'){
      Actions.setting();
    }else{
      Actions.home();
    }
  }
  render() {

    return (
      <Footer >
            <FooterTab>
            <Button active={(this.state.activeTabName==='home') ? true: false} onPress={()=>{this.tabAction('home')}}>
                <Icon name="home"/>
                <Text>Home</Text>
            </Button>
            <Button active={(this.state.activeTabName==='explorer') ? true: false} onPress={()=>{this.tabAction('explorer')}}>
                <Icon name="globe"/>
                <Text>Explorer</Text>
            </Button>
            <Button active={(this.state.activeTabName==='setting') ? true: false} onPress={()=>{this.tabAction('setting')}}>
                <Icon name="settings"/>
                <Text>Setting</Text>
            </Button>
            </FooterTab>
        </Footer>
    );
  }
}

function bindAction(dispatch) {
  return {
  };
}

const mapStateToProps = state => ({
});


export default connect(mapStateToProps, bindAction)(FooterPage);
