import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions,ActionConst} from 'react-native-router-flux';
import {View,Text,Image,StatusBar} from 'react-native';
import AppIntro from 'react-native-app-intro';
import { skipIntro } from '../../actions/user';
import { statusBarColor } from '../../themes/base-theme';

import styles from './styles';
var bgColor = ['#006699','#a4b602'];

class Intro extends Component {

  constructor(props) {
    super(props);
    this.state ={
      flag:false,
    }
  }

  _setIntro(flag) {
    this.props.skipIntro(flag);
  }

  _onSkipBtnHandle = (index) => {
    this._setIntro(true);
    Actions.login();
  };
  _doneBtnHandle =  () => {
    Actions.login();
  };
  _onSlideChangeHandle = (index,total)=>{
    // StatusBar.setBackgroundColor(bgColor[index],true);
  };
  render() {
    return (
      <AppIntro
        onNextBtnClick={this._nextBtnHandle}
        onDoneBtnClick={this._doneBtnHandle}
        onSkipBtnClick={this._onSkipBtnHandle}
        onSlideChange={this._onSlideChangeHandle}
        doneBtnLabel={"Enter"}
      >
        <View style={[styles.slide,{ backgroundColor:  bgColor[0]}]}>
          <View style={styles.image} level={15}>
            <Image source={require('../../../images/intro/1.png')} />
          </View>
          <View level={8}><Text style={styles.subtext2}>Welcome to Your #1 Peer-to-Peer Marketplace Overseas.</Text></View>
        </View>

        <View style={[styles.slide, { backgroundColor: bgColor[1]}]}>
            <View style={styles.image} level={-10}>
              <Image source={require('../../../images/intro/4.png')} />
            </View>
          <View level={5}><Text style={styles.subtext2}>Marketplace for everyone to get products overseas.</Text></View>
        </View>

      </AppIntro>
    );
  }
}

function bindAction(dispatch) {
  return {
    skipIntro: flag => dispatch(skipIntro(flag)),
  };
}
const mapStateToProps = state => ({

});

export default connect(mapStateToProps, bindAction)(Intro);
