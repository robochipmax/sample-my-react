const React = require('react-native');

const { StyleSheet,Dimensions} = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default{
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
    padding: 15,
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
    textAlign:'center'

  },
  subtext:{
    color:'#FFF',
    fontSize:15,
    alignItems:'center',
    textAlign:'center'
  },
  subtext2:{
    color:'#FFF',
    fontSize:18,
    alignItems:'center',
    textAlign:'center',
    marginTop:250,
  },
  image:{
    position: 'absolute',
    top: 60,
    width: deviceWidth,
    height: deviceHeight,
    alignItems:'center'
  },
  topimage:{
    position:'absolute',
    width:deviceWidth,
    height:deviceHeight,
    top:20,
    alignItems:'center'
  }
};
