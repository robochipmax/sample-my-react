
const React = require('react-native');

const { StyleSheet,Dimensions } = React;
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  containerTop:{
    paddingTop:20,
    paddingBottom:20
  },
  container:{
      backgroundColor:'#efefef',
      width:deviceWidth,
      height:deviceHeight,
  },
  wrapperSlider: {
    height:200,
    flex:0,
    justifyContent:'flex-start',
    alignItems:'stretch'
   },

   sliderItem:{
     width:deviceWidth,
   },
  sliderItemThumb:{
    height:deviceHeight/4,
    width:deviceWidth/3.5,
    backgroundColor:'#FFF',
    marginRight:5,
    borderRadius:4,
    flex:0,
    justifyContent:'flex-start',
    alignItems:'stretch'
  },
  sliderItemImg:{
    flex:1,
    margin:5,
    borderRadius:4,
  },
  sliderItemTextWrapper:{
    margin:5,
  },
  sliderItemText:{
    color:'#000',
    fontSize:12,
    height:17,
  },
   title: {
     color: '#006699',
     fontSize: 13,
     marginBottom:5,
   },
   titleRight:{
     textAlign:'right',
   },
   titleBlue:{
     backgroundColor:'blue',
     borderRadius:10,
     color:'#fff',
     paddingLeft:10,
     paddingBottom:2,
     textAlign:'left',
   }
};
