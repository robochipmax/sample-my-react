import React, { Component } from 'react';
import { TouchableOpacity,Image,ScrollView,StatusBar} from 'react-native';
import { connect } from 'react-redux';
import {
  Actions,
   ActionConst
 } from 'react-native-router-flux';
import {
  StyleProvider,
  Item,Input,
  Container,
  Header,
  Title,
  Content,
  Tabs,Tab,Text,
  Button,
  Icon,
  Left,View,
  Body,
  Spinner} from 'native-base';
import {
  Grid,
  Row,
  Col } from 'react-native-easy-grid';

import { openDrawer } from '../../actions/drawer';
import styles from './styles';
import getTheme from '../../../native-base-theme/components';
import theme from '../../themes/base-theme';
import api from '../../byfus';
class Main extends Component {

  static propTypes = {
    list: React.PropTypes.object,
    openDrawer: React.PropTypes.func,
  }

constructor(props){
  super(props);
  this.state ={
    banner : {isLoading:true,page:0,items:[]},
    recommendation:{isLoading:true,page:0,items:[]},
    latest :{isLoading:true,page:0,items:[]},
    limit:5
  }
}
 async getLatest(){
   try{
     this.setState({latest:{isLoading:true,page:this.state.latest.isLoading,page:this.state.latest.page,items:this.state.latest.items}});
     api._get('request?recommend=0&page='+this.state.latest.page+'&limit='+this.state.limit).then((res) =>{
       if(res.status) {
             this.setState({
                latest:{
                items:this.state.latest.items.concat(res.message),
                isLoading:false,
                page:this.state.latest.page+1,
               }
             });
       }else{
         this.setState({latest:{isLoading:false,page:this.state.latest.isLoading,page:this.state.latest.page,items:this.state.latest.items}});
       }
     });
   }catch(error){
     console.error(error);
   }
 }

 async getBanner(){
   try{
     this.setState({banner:{isLoading:true,page:this.state.banner.isLoading,page:this.state.banner.page,items:this.state.banner.items}});
     api._get('banner').then((res) =>{
       if(res.status) {
         this.setState({
            banner:{
            items:this.state.banner.items.concat(res.message),
            isLoading:false,
            page:this.state.banner.page+1,
           }
         });
       }else{
         this.setState({banner:{isLoading:false,page:this.state.banner.isLoading,page:this.state.banner.page,items:this.state.banner.items}});
       }
     });
   }catch(error){
     console.error(error);
   }

 }
 async getRecommend(){
   try{
     this.setState({recommendation:{isLoading:true,page:this.state.recommendation.page,items:this.state.recommendation.items}});
     await api._get('request?recommend=1&page='+(this.state.recommendation.page*this.state.limit)+'&limit='+this.state.limit).then((res) =>{
       if(res.status){
        var tempItem = this.state.recommendation.items;
         this.setState({
           recommendation:{
            items:this.state.recommendation.items.concat(res.message),
            isLoading:false,
            page:this.state.recommendation.page+1,
           }
         });
       }else{
         this.setState({recommendation:{isLoading:false,page:this.state.recommendation.page,items:this.state.recommendation.items}});
       }
     });
   }catch(error){
     console.error(error);
   }

 }
  componentWillMount(){
     this.getBanner();
     this.getRecommend();
     this.getLatest();
  }

 _renderBaner(){
   if (this.state.banner.isLoading) {
      return <Spinner key={Math.random()}/>;
    }
   return this.state.banner.items.map(function(data,index){
     return (
       <Image key={index} source={{uri:api.url+data.image}} style={styles.sliderItem}/>
     )
   });
 }

 _renderRecommend(){
   if (this.state.recommendation.isLoading && this.state.recommendation.page==0) {
      return <Spinner key={Math.random()}/>;
    }

    return this.state.recommendation.items.map(function(data,index){
     return (
       <TouchableOpacity  key={index}  onPress={() => { Actions.request_detail(data) }} >
       <View style={styles.sliderItemThumb}>
         <Image source={{uri: api.asset_upload+data.thumbnail}} style={styles.sliderItemImg}/>
         <View style={styles.sliderItemTextWrapper}>
           <Text style={styles.sliderItemText}>{data.name}</Text>
           <Text style={styles.sliderItemText}>{data.requestor_name}</Text>
           <Text style={styles.sliderItemText}>{data.total_fee_formated}</Text>
         </View>
       </View>
       </TouchableOpacity>
     )
   });
 }

 _renderLatest(){
   if (this.state.latest.isLoading && this.state.recommendation.page==0) {
      return <Spinner key={Math.random()}/>;
    }

   return this.state.latest.items.map(function(data,index){
     return (
       <TouchableOpacity key={index} onPress={() => { Actions.request_detail(data) }} >
       <View style={styles.sliderItemThumb}>
         <Image source={{uri: api.asset_upload+data.thumbnail}} style={styles.sliderItemImg}/>
         <View style={styles.sliderItemTextWrapper}>
           <Text style={styles.sliderItemText}>{data.name}</Text>
           <Text style={styles.sliderItemText}>{data.requestor_name}</Text>
           <Text style={styles.sliderItemText}>{data.total_fee_formated}</Text>
         </View>
       </View>
       </TouchableOpacity>
     )
   });
 }
 getDistance(e){
   let {
      contentSize,
      contentInset,
      contentOffset,
      layoutMeasurement,
    } = e.nativeEvent;

   contentLength = contentSize.width;
   trailingInset = contentInset.right;
   scrollOffset = contentOffset.x;
   viewportLength = layoutMeasurement.width;
   var distance = contentLength + trailingInset - scrollOffset - viewportLength;
   return distance;
 }
 onScrollRecommendation(e) {
  if(!this.state.recommendation.isLoading && this.getDistance(e) <=0){
      this.getRecommend()
  }
 }

 onScrollLatest(e) {
    if(!this.state.latest.isLoading && this.getDistance(e) <=0){
        this.getLatest()
    }
 }

   recommendScrollEnd(contentWidth, contentHeight) {
         if(this.state.recommendation.page > 1) this.scrollViewRecommend.scrollTo({x:contentWidth/(this.state.recommendation.page+1)});
  }
  latestScrollEnd(contentWidth, contentHeight) {
        if(this.state.latest.page > 1) this.scrollViewLatest.scrollTo({x:contentWidth/(this.state.latest.page+1)});
 }
  render() {
    return (
      <StyleProvider style={getTheme(theme)}>
      <Container >
        <Header style={styles.containerTop}
        searchBar>
            <Item>
                <Icon active name="menu" onPress={this.props.openDrawer}/>
                <Input placeholder="Search" />
                <Icon name="ios-search" />
            </Item>
            <Button transparent>
                <Text>Search</Text>
            </Button>
        </Header>
        <Content>
        <View style={styles.container}>
        <View style={styles.wrapperSlider} >
        <ScrollView
        contentContainerStyle={this.state.banner.isLoading ? {flex:1,justifyContent:'center',alignItems:'center'}:{}}
        horizontal={true} showsHorizontalScrollIndicator={false} pagingEnabled={true}>
        {this._renderBaner()}
        </ScrollView>
        </View>
        <View  padder={true}>
        <View style={{height:30}}>
            <Grid>
                <Col><Text style={styles.title}>Recommendation</Text></Col>
                <Col><Text style={styles.title,styles.titleRight}>More</Text></Col>
            </Grid>
          </View>
            <ScrollView
              ref={(ref)=>{this.scrollViewRecommend = ref}}
              onContentSizeChange={this.recommendScrollEnd.bind(this)}
              contentContainerStyle={this.state.recommendation.isLoading && this.state.recommendation.page==0 ? {flex:1,justifyContent:'center',alignItems:'center'}:{}}
              scrollEventThrottle={300}
              onScroll={this.onScrollRecommendation.bind(this)}
              horizontal={true} showsHorizontalScrollIndicator={false} pagingEnabled={false}>
              {this._renderRecommend()}
            </ScrollView>
        </View>
        <View  padder={true}>
          <View style={{height:30}}>
              <Grid>
                  <Col><Text style={styles.title}>Latest</Text></Col>
                  <Col><Text style={Object.assign(styles.titleRight,styles.title)}>More</Text></Col>
              </Grid>
            </View>
            <ScrollView
            ref={(ref)=>{this.scrollViewLatest = ref}}
            onContentSizeChange={this.latestScrollEnd.bind(this)}
            scrollEventThrottle={100}
            onScroll={this.onScrollLatest.bind(this)}
            contentContainerStyle={this.state.latest.isLoading && this.state.latest.page==0  ? {flex:1,justifyContent:'center',alignItems:'center'}:{}}
            horizontal={true} showsHorizontalScrollIndicator={false} pagingEnabled={false}>
            {this._renderLatest()}
            </ScrollView>
        </View>
        </View>
        </Content>
      </Container>
      </StyleProvider>
    );
  }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
  };
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps, bindAction)(Main);
