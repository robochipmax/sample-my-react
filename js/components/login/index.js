import React, { Component } from 'react';
import { Image,StatusBar,Alert,ActivityIndicator} from 'react-native';
import { connect } from 'react-redux';
import { Container, Content,H1,Grid,Col, Item, Input, Button, Icon, View, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconAwe from 'react-native-vector-icons/FontAwesome';
import styles from './styles';
import {signIn} from '../../actions/user';
import api from '../../byfus';
import { statusBarColor } from '../../themes/base-theme';

const logo = require('../../../images/intro/1.png');

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
       isLoading: false,
       email: 'robochipmax@gmail.com',
       password: '87654321',
       flag:false
     }
  }

  login() {
    this.setState({
      isLoading:false
    });
    api._post('auth/login',{identity:this.state.email,password:this.state.password}).then((res) =>{
        if(res.status){
            this.props.signIn(res.message);
        }else{
          Alert.alert(
            'Byfus',
            res.message
          )
        }
    });
  }

  render() {
    return (
      <Container>
        <View style={styles.container}>
          <Content>
          <Grid  style={styles.skipLogin}>
              <Col>
              <Button transparent  block  iconLeft onPress={() => Actions.intro({direction:'leftToRight'})}>
                <Icon name="ios-arrow-back"/>
                <Text >Back to Intro</Text>
              </Button>
              </Col>
              <Col>
              <Button transparent  full  iconRight onPress={() => this.props.skipLogin()} >
                <Text  >Skip Login</Text>
                <Icon name="ios-arrow-forward"/>
              </Button>
              </Col>
          </Grid>
            <View style={styles.centered}><Image source={logo} style={styles.logo}/></View>
              <View style={styles.bg}>
                <Item style={styles.input} first>
                  <Icon active name="person" />
                  <Input placeholder="Email"
                  onChangeText={(text) => this.setState({email: text})}
                />
                </Item>
                <Item style={styles.input}>
                  <Icon name="unlock" />
                  <Input
                    onChangeText={(text) => this.setState({password: text})}
                    placeholder="Password"
                    secureTextEntry
                  />
                </Item>

                <Button full light  style={styles.btn} onPress={() => this.login()}>
                  <Text>Login</Text>
                </Button>

                <View style={styles.btn}>
                  <H1 style={styles.h1}> or </H1>
                </View>

                <Button iconLeft full light   style={styles.facebookBtn} onPress={() => Actions.home()}>
                  <Text style={styles.white}>Login via Facebook</Text>
                </Button>
                <Grid  style={styles.btn}>
                    <Col>
                    <Button transparent  light onPress={() => Actions.register()}>
                      <Text>Create an account</Text>
                    </Button>
                    </Col>
                    <Col>
                    <Button transparent light onPress={() => Actions.forget()}>
                      <Text>Forget Password ?</Text>
                    </Button>
                    </Col>
                </Grid>
              </View>
          </Content>
        </View>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    signIn: userdata => dispatch(signIn(userdata)),
  };
}

const mapStateToProps = state => ({
  selected:state.selected
});
export default connect(mapStateToProps, bindActions)(Login);
