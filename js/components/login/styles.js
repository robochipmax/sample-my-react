
const React = require('react-native');

const { StyleSheet, Dimensions } = React;
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  logo: {
      position:'absolute',
      top:deviceHeight / 20,
  },
  centered:{
    alignItems:'center',
  },
  skipLogin:{
    position:'absolute',
    flex:1,
    justifyContent:'center',
    alignItems:'stretch'
  },
  bg: {
    flex: 1,
    marginTop: deviceHeight / 3,
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 30,
    bottom: 0,
  },
  input: {
    marginBottom: 20,
  },
  btn: {
    marginTop: 10,
  },
  h1:{
    alignItems:'center',
    textAlign:'center',
  },
  white:{
    color:'#fff'
  },
  facebookBtn:{
    backgroundColor:'#3b5998',
    marginTop: 10,
  },
};
