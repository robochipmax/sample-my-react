
import React, { Component } from 'react';
import { TouchableOpacity,Image } from 'react-native';
import { connect } from 'react-redux';
import { Actions, ActionConst } from 'react-native-router-flux';
import { StyleProvider, Container, Header, Title, Content, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Grid, Row } from 'react-native-easy-grid';

import { openDrawer } from '../../actions/drawer';
import styles from './styles';
import getTheme from '../../../native-base-theme/components';
import theme from '../../themes/base-theme';
import FooterPage from '../footerPage/';
class Request extends Component {

  static propTypes = {
    setIndex: React.PropTypes.func,
    openDrawer: React.PropTypes.func,
  }

  newPage(index) {
    Actions.blankPage();
  }

  render() {
    return (
      <StyleProvider style={getTheme(theme)}>
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={this.props.openDrawer}>
              <Icon active name="menu" />
            </Button>
          </Left>

          <Body>
            <Title>Request</Title>
          </Body>

        </Header>

        <Content>

        </Content>

      </Container>
      </StyleProvider>
    );
  }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
  };
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps, bindAction)(Request);
