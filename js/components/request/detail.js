
import React, { Component } from 'react';
import { TouchableOpacity,Image } from 'react-native';
import { connect } from 'react-redux';
import { Actions, ActionConst } from 'react-native-router-flux';
import { StyleProvider, Container, Header, Title, Content, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Grid, Row } from 'react-native-easy-grid';

import { openDrawer } from '../../actions/drawer';
import styles from './styles';
import getTheme from '../../../native-base-theme/components';
import theme from '../../themes/base-theme';
import FooterPage from '../footerPage/';
import api from '../../byfus';
class RequestDetail extends Component {

  static propTypes = {
    setIndex: React.PropTypes.func,
    openDrawer: React.PropTypes.func,
  }

  constructor(props){
    super(props);
    console.log(this.props);
  }
  render() {
    return (
      <StyleProvider style={getTheme(theme)}>
      <Container style={styles.container}>
        <Content>
        <Image source={{uri:api.asset_upload+this.props.thumbnail}} style={{flexDirection:'column',height:320,justifyContent:'flex-end',padding:10,alignSelf:'stretch'}}/>
        </Content>

      </Container>
      </StyleProvider>
    );
  }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
  };
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps, bindAction)(RequestDetail);
