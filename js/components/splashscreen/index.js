import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View,Image,StatusBar } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { statusBarColor } from '../../themes/base-theme';

const launchscreen = require('../../../images/splash-logo.png');

class SplashPage extends Component {

  static propTypes = {
  }
  constructor(props) {
    super(props);
    StatusBar.setBackgroundColor("#36c6d3");
    StatusBar.setBarStyle("light-content");
  }
  componentDidMount() {
    setTimeout(() => {
      if(this.props.user.hasSkipedIntro){
        if(this.props.user.hasSkipedIntro==false && this.props.user.hasLoggedIn==true || this.props.user.hasSkippedLogin) {
        //  Actions.main();
        }else{
          Actions.login();
        }
      }else{
        Actions.login();
      }
    }, 1500);
  }

  render() {
    return (
      <View  style={{ backgroundColor:'#36c6d3',flex:1,alignItems:'center',justifyContent:'center',height:null,width:null}}>
      <Image source={launchscreen}  style={{flex:0}}/>
      </View>
    );
  }
}
const mapStateToProps = state => ({
  user:state.user,
});


export default connect(mapStateToProps, null)(SplashPage);
