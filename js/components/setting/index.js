
import React, { Component } from 'react';
import { TouchableOpacity,Image } from 'react-native';
import { connect } from 'react-redux';
import { Actions, ActionConst } from 'react-native-router-flux';
import { StyleProvider, Container, Header, Title,ListItem, Content, Text, Switch,Button, Icon, Left, Body, Right } from 'native-base';
import { Grid, Row } from 'react-native-easy-grid';

import { openDrawer } from '../../actions/drawer';
import styles from './styles';
import getTheme from '../../../native-base-theme/components';
import theme from '../../themes/base-theme';
import FooterPage from '../footerPage/';
class Setting extends Component {

  static propTypes = {
    setIndex: React.PropTypes.func,
    openDrawer: React.PropTypes.func,
  }

  newPage(index) {
    Actions.blankPage();
  }

  render() {
    return (
      <StyleProvider style={getTheme(theme)}>
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={this.props.openDrawer}>
              <Icon active name="menu" />
            </Button>
          </Left>

          <Body>
            <Title>Setting</Title>
          </Body>

        </Header>

        <Content>
        <ListItem icon>
                <Left>
                    <Icon name="plane" />
                </Left>
                <Body>
                  <Text>Airplane Mode</Text>
                </Body>
                <Right>
                    <Switch value={false} />
                </Right>
            </ListItem>
            <ListItem icon>
                            <Left>
                                <Icon name="wifi" />
                            </Left>
                            <Body>
                              <Text>Wi-Fi</Text>
                            </Body>
                            <Right>
                                <Text>GeekyAnts</Text>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Icon name="bluetooth" />
                            </Left>
                            <Body>
                              <Text>Bluetooth</Text>
                            </Body>
                            <Right>
                                <Text>On</Text>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>
        </Content>

      </Container>
      </StyleProvider>
    );
  }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
  };
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps, bindAction)(Setting);
