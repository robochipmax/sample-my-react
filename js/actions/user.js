import type { Action } from './types';

export const LOGGED_IN = 'LOGGED_IN';
export const LOGGED_OUT = 'LOGGED_OUT';
export const SKIP_INTRO = 'SKIP_INTRO';
export const SKIP_LOGIN = "SKIP_LOGIN";
export function signIn(user:object):Action {
  return {
    type: LOGGED_IN,
    payload: user,
  };
}
export function skipIntro(flag:boolean):Action {
  return {
    type: SKIP_INTRO,
    payload: flag,
  };
}
export function skipLogin(flag:boolean):Action {
  return {
    type: SKIP_LOGIN,
    payload: flag,
  };
}
export function logout():Action {
  return {
    type: LOGGED_OUT,
  };
}
